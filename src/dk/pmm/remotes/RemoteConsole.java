package dk.pmm.remotes;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTInfo;
import dk.pmm.DeviceConstants;
import dk.pmm.communication.RemoteServer;
import dk.pmm.utilities.Csv;

public class RemoteConsole {

	public static void main(String[] args) throws Exception {
		// create device information for the device we want to connect to
		NXTInfo info = new NXTInfo(NXTCommFactory.BLUETOOTH,
				DeviceConstants.NAME, DeviceConstants.ADDRESS);
		// construct the new remote server
		final RemoteServer remote = new RemoteServer() {

			@Override
			protected void onMessageCommand(String message) {
				System.err.println(message);
			};

			@Override
			protected void log(int channel, Object... elements) {
				System.out.println("[" + channel + "] " + Csv.csvRecord(elements));
			}

		};
		// setup the remote
		remote.setup();
		// try and connect
		if (remote.connect(info)) {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));
			String line;
			while ((line = br.readLine()) != null) {
				if ("exit".equals(line)) {
					break;
				} else if ("kill".equals(line)) {
					remote.kill();
					break;
				} else {
					remote.message(line);
				}
			}
		} else {
			System.out.println("Did not connect to the device.");
		}
		// disconnect the connection
		remote.disconnect();
		// print closing message
		System.out.println("Closed nicely");
	}

}