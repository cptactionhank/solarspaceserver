package dk.pmm.remotes;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import lejos.pc.comm.NXTCommException;
import lejos.pc.comm.NXTInfo;
import dk.pmm.DeviceConstants;
import dk.pmm.communication.RemoteServer;

/**
 * A GUI that makes it possible to establish a Bluetooth connection to an NXT
 * controlled car. The car is then controlled by a power and duration value sent
 * to the car. The tacho counter value is received and displayed after each
 * command to the car has been performed.
 * 
 * @author Martin Jensen
 */
public class RemoteGui extends RemoteServer implements Runnable {

	private final NXTInfo nxtInfo = new NXTInfo();
	private final JFrame frame = new JFrame();

	private final TextField nameField = new TextField(12);
	private final TextField addressField = new TextField(20);

	private final TextField offsetField = new TextField(10);
	private final TextField valueField = new TextField(10);
	private final TextField kpField = new TextField(10);
	private final TextField kiField = new TextField(10);
	private final TextField kdField = new TextField(10);
	private final TextField scaleField = new TextField(10);

	private final JButton connectButton = new JButton("Connect");
	private final JButton goButton = new JButton("Send");

	/**
	 * Constructor builds GUI
	 * 
	 * @throws NXTCommException
	 */
	public RemoteGui(String address, String name) throws Exception {
		// pre-initialize device info
		nxtInfo.deviceAddress = address;
		nxtInfo.name = name;

		// initialize frame options
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Control NXT");
		frame.setSize(500, 300);

		// holds labels and text fields
		JPanel p1 = new JPanel();
		p1.add(new JLabel("Name:"));
		p1.add(nameField);
		nameField.setText(nxtInfo.name);
		JPanel p11 = new JPanel();
		p11.add(new JLabel("Address:"));
		p11.add(addressField);
		addressField.setText(nxtInfo.deviceAddress);

		// holds connect button
		JPanel p2 = new JPanel();
		p2.add(connectButton);
		connectButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!connect(nxtInfo))
					JOptionPane.showMessageDialog(frame, "Did not connect",
							frame.getTitle(), JOptionPane.WARNING_MESSAGE);
			}
		});

		// holds labels and text fields
		JPanel p3 = new JPanel();
		p3.add(new JLabel("Offset:"));
		p3.add(offsetField);
		offsetField.setText("55");

		JPanel p7 = new JPanel();
		p7.add(new JLabel("KP:"));
		p7.add(kpField);
		kpField.setText("78");

		JPanel p8 = new JPanel();
		p8.add(new JLabel("KI:"));
		p8.add(kiField);
		kiField.setText("0.0334");

		JPanel p9 = new JPanel();
		p9.add(new JLabel("KD:"));
		p9.add(kdField);
		kdField.setText("65000");

		JPanel p10 = new JPanel();
		p10.add(new JLabel("SCALE:"));
		p10.add(scaleField);
		scaleField.setText("18");

		// holds go button
		JPanel p4 = new JPanel();
		p4.add(goButton);
		goButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					messenger.vendorCommand(0,
							new Integer(offsetField.getText()));
					messenger.vendorCommand(1, new Integer(kpField.getText()));
					messenger.vendorCommand(2, new Float(kiField.getText()));
					messenger.vendorCommand(3, new Integer(kdField.getText()));
					messenger.vendorCommand(4,
							new Integer(scaleField.getText()));
					messenger.vendorCommand(5,
							new Integer(valueField.getText()));
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});

		// holds labels and text field
		JPanel p5 = new JPanel();
		p5.add(new JLabel("Power:"));
		p5.add(valueField);
		valueField.setText("70");

		// North area of the frame
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(5, 1));
		panel.add(p1);
		panel.add(p11);
		panel.add(p2);
		panel.add(p3);
		panel.add(p7);
		panel.add(p8);
		panel.add(p9);
		panel.add(p10);
		panel.add(p4);
		panel.add(p5);
		frame.add(panel, BorderLayout.NORTH);
	}

	@Override
	public void run() {
		frame.setVisible(true);		
	}

	public static void main(String[] args) throws Exception {
		RemoteGui gui = new RemoteGui(DeviceConstants.ADDRESS,
				DeviceConstants.NAME);
		gui.setup();
		gui.run();
	}

}