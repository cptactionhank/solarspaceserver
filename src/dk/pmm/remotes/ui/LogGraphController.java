package dk.pmm.remotes.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.time.TimeSeriesDataItem;
import org.jfree.data.xy.XYDataset;

import dk.pmm.communication.RemoteServer;
import dk.pmm.messaging.events.MessagingEventObserver;

@SuppressWarnings("serial")
public class LogGraphController extends JFrame {

	private JPanel contentPane;
	private ChartPanel chartPanel;

	private final TimeSeriesCollection dataset = new TimeSeriesCollection();
	private final TimeSeries[] series;
	private final MessagingEventObserver observer;
	private final RemoteServer remote;

	public LogGraphController(final RemoteServer remote,
			final int listenChannel, int... indices) {
		// find max index
		int max = -1;
		for (int index : indices)
			max = Math.max(max, index);
		// create series array
		series = new TimeSeries[max + 1];
		// fill series where something
		int count = 1;
		for (int index : indices) {
			series[index] = new TimeSeries("series " + count++);
			dataset.addSeries(series[index]);
		}
		// save reference to remote server
		this.remote = remote;
		// create remote listener
		observer = new MessagingEventObserver() {

			@Override
			public void log(int channel, Object... elements) {
				// extract timestamp
				long timestamp = (Long) elements[0];
				// filter other channels
				if (listenChannel == channel) {
					// iterate all log values
					for (int i = 0; i < series.length; i++) {
						// filter elements
						if (series[i] != null && i < elements.length) {
							// log data value
							TimeSeries ts = series[i];
							Object value = elements[i];
							// but only if it's a number
							if (ts != null && value instanceof Number) {
								ts.add(new TimeSeriesDataItem(new Millisecond(
										new Date(timestamp)), (Number) value));
							}
						}
					}
				}
			}
		};

		final JFreeChart chart = createChart(dataset);

		setTitle("PID Remote Controller");
		setType(Type.UTILITY);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 538, 355);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		chartPanel = new ChartPanel(chart);
		contentPane.add(chartPanel, BorderLayout.CENTER);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JButton btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				chart.setNotify(false);
				for (TimeSeries ts : series)
					if (ts != null)
						ts.clear();
				chart.setNotify(true);
			}
		});
		panel.add(btnClear);

		JButton btnSaveImage = new JButton("Save image");
		btnSaveImage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					ChartUtilities.saveChartAsPNG(
							File.createTempFile("chart-", ".png", new File("")), chart, 538,
							355);
				} catch (IOException e1) {
				}
			}
		});
		panel.add(btnSaveImage);
		
		JButton btnSaveCsv = new JButton("Save CSV");
		btnSaveCsv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Not implemented");
			}
		});
		panel.add(btnSaveCsv);
	}

	@Override
	public void setVisible(boolean b) {
		super.setVisible(b);
		if (b) {
			remote.addObserver(observer);
		} else {
			remote.removeObserver(observer);
		}
	}

	private JFreeChart createChart(final XYDataset dataset) {
		final JFreeChart result = ChartFactory.createTimeSeriesChart("",
				"Time", "Value", dataset, true, true, false);
		final XYPlot plot = result.getXYPlot();
		ValueAxis axis = plot.getDomainAxis();
		axis.setAutoRange(true);
		axis.setFixedAutoRange(60000.0); // 60 seconds
		axis = plot.getRangeAxis();
		result.setAntiAlias(true);
		return result;
	}

}
