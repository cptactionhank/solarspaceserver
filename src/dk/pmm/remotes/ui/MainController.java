package dk.pmm.remotes.ui;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.border.EmptyBorder;

import lejos.pc.comm.NXTCommException;
import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTInfo;
import dk.pmm.DeviceConstants;
import dk.pmm.communication.RemoteServer;
import dk.pmm.messaging.MessagingConstants;
import dk.pmm.messaging.events.MessagingEventObserver;
import dk.pmm.utilities.Csv;

@SuppressWarnings("serial")
public class MainController extends JFrame implements Runnable {

	private JPanel contentPane;
	private JTextField txtName;
	private JTextField txtAddress;

	private final MessagingEventObserver consoleLogger = new MessagingEventObserver() {
		@Override
		public void log(int channel, Object... elements) {
			System.out.println("[" + channel + "]: " + Csv.csvRecord(elements));
		}
	};

	/**
	 * Launch the application.
	 * 
	 * @throws NXTCommException
	 */
	public static void main(String[] args) throws Exception {

		// final RemoteServer server = new RemoteServer();
		final MainController frame = new MainController(new NXTInfo(
				NXTCommFactory.BLUETOOTH, DeviceConstants.NAME,
				DeviceConstants.ADDRESS));
		// initialize remote server
		// server.setup();
		// Show the
		EventQueue.invokeLater(frame);
	}

	/**
	 * Create the frame.
	 * 
	 * @throws NXTCommException
	 */
	public MainController(final NXTInfo info) throws NXTCommException {
		final PrintWriter[] logWriters = new PrintWriter[MessagingConstants.MAX_LOG_CHANNELS];
		final RemoteServer remote = new RemoteServer() {

			@Override
			protected void onMessageCommand(String message) {
				System.err.println(message);
			}

			@Override
			protected void log(int channel, Object... elements) {
				PrintWriter pw = null;
				// get or create new file for logging
				synchronized (logWriters) {
					pw = logWriters[channel];
					if (pw == null) {
						// try and create a new log file
						try {
							File file = File.createTempFile(" channel"
									+ channel, ".csv");
							// create writer
							pw = logWriters[channel] = new PrintWriter(file);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
				// write the log to filesystem
				if (pw != null) {
					pw.println(Csv.csvRecord(elements));
					pw.flush();
				}
			}

		};
		// create UI
		setTitle("Remote Controller");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 401);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 10, 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0,
				Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		JPanel panel_1 = new JPanel();
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.insets = new Insets(0, 0, 5, 0);
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 0;
		contentPane.add(panel_1, gbc_panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[] { 0, 0, 0 };
		gbl_panel_1.rowHeights = new int[] { 0, 0, 0, 0 };
		gbl_panel_1.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gbl_panel_1.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panel_1.setLayout(gbl_panel_1);

		JLabel lblName = new JLabel("Name");
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.anchor = GridBagConstraints.EAST;
		gbc_lblName.gridx = 0;
		gbc_lblName.gridy = 0;
		panel_1.add(lblName, gbc_lblName);

		txtName = new JTextField(info.name);
		GridBagConstraints gbc_txtName = new GridBagConstraints();
		gbc_txtName.insets = new Insets(0, 0, 5, 0);
		gbc_txtName.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtName.gridx = 1;
		gbc_txtName.gridy = 0;
		panel_1.add(txtName, gbc_txtName);
		txtName.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("Address");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 1;
		panel_1.add(lblNewLabel_1, gbc_lblNewLabel_1);

		txtAddress = new JTextField(info.deviceAddress);
		GridBagConstraints gbc_txtAddress = new GridBagConstraints();
		gbc_txtAddress.insets = new Insets(0, 0, 5, 0);
		gbc_txtAddress.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtAddress.gridx = 1;
		gbc_txtAddress.gridy = 1;
		panel_1.add(txtAddress, gbc_txtAddress);
		txtAddress.setColumns(10);

		JPanel panel_2 = new JPanel();
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.anchor = GridBagConstraints.EAST;
		gbc_panel_2.fill = GridBagConstraints.VERTICAL;
		gbc_panel_2.gridx = 1;
		gbc_panel_2.gridy = 2;
		panel_1.add(panel_2, gbc_panel_2);
		panel_2.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JButton btnNewButton_3 = new JButton("Disconnect");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				remote.disconnect();
			}
		});
		panel_2.add(btnNewButton_3);

		JButton btnNewButton_2 = new JButton("Connect");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// update info object
				info.name = txtName.getText();
				info.deviceAddress = txtAddress.getText();
				// connect to the device
				if (!remote.connect(info))
					JOptionPane.showMessageDialog(null, "Did not connect",
							getTitle(), JOptionPane.WARNING_MESSAGE);
			}
		});
		panel_2.add(btnNewButton_2);

		JLabel lblRemoteContriks = new JLabel("Remote Controls");
		lblRemoteContriks.setFont(new Font("Tahoma", Font.BOLD, 13));
		GridBagConstraints gbc_lblRemoteContriks = new GridBagConstraints();
		gbc_lblRemoteContriks.insets = new Insets(0, 0, 5, 0);
		gbc_lblRemoteContriks.gridx = 0;
		gbc_lblRemoteContriks.gridy = 2;
		contentPane.add(lblRemoteContriks, gbc_lblRemoteContriks);

		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.anchor = GridBagConstraints.NORTH;
		gbc_panel.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 3;
		contentPane.add(panel, gbc_panel);
		panel.setLayout(new GridLayout(0, 1, 100, 0));

		JButton btnRemoteController = new JButton("Remote Controller");
		btnRemoteController.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RemoteMotorController controller = new RemoteMotorController(
						remote.getMessenger());
				controller.setVisible(true);
			}
		});

		JButton btnShutdown = new JButton("Shutdown");
		btnShutdown.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				remote.kill();
			}
		});

		JToggleButton btnLogToConsole = new JToggleButton("Log to console");
		btnLogToConsole.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JToggleButton tgl = (JToggleButton) e.getSource();
				if (tgl.getModel().isSelected()) {
					remote.addObserver(consoleLogger);
				} else {
					remote.removeObserver(consoleLogger);
				}
			}
		});
		panel.add(btnLogToConsole);
		panel.add(btnShutdown);
		panel.add(btnRemoteController);

		JButton btnPidController = new JButton("PID Controller");
		btnPidController.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PidController controller = new PidController(remote);
				controller.setVisible(true);
			}
		});
		panel.add(btnPidController);

		JButton btnGraph = new JButton("Graph");
		btnGraph.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String channelString = JOptionPane.showInputDialog(
						"Please write channel", "0");
				String indicesString = JOptionPane
						.showInputDialog("Please write indices comma separated");
				String[] indicesStringArray = indicesString.split(",");
				
				int channel = new Integer(channelString);
				int[] indices = new int[indicesStringArray.length];
				for (int i = 0; i < indicesStringArray.length; i++)
					indices[i] = new Integer(indicesStringArray[i]);
				
				LogGraphController controller = new LogGraphController(remote,
						channel, indices);
				controller.setVisible(true);
			}
		});
		panel.add(btnGraph);
	}

	@Override
	public void run() {
		setVisible(true);
	}

}
