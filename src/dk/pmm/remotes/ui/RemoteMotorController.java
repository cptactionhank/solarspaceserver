package dk.pmm.remotes.ui;

import java.awt.BorderLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import dk.pmm.ControlConstants;
import dk.pmm.controls.RemoteDirectionalControl;
import dk.pmm.messaging.Messenger;

@SuppressWarnings("serial")
public class RemoteMotorController extends JFrame {

	private JPanel contentPane;

	public RemoteMotorController(final Messenger messenger) {
		// create directional controller
		final RemoteDirectionalControl remote = new RemoteDirectionalControl(
				ControlConstants.OFFSET_REMOTE, messenger);
		// listen for incoming commands
		messenger.addObserver(remote);

		addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent e) {
				int code = e.getKeyCode();
				// skip if it's a repeat
				if (repeatPrevent(code))
					return;
				// handle events
				if (code == KeyEvent.VK_UP) {
					remote.north();
				} else if (code == KeyEvent.VK_DOWN) {
					remote.south();
				} else if (code == KeyEvent.VK_LEFT) {
					remote.west();
				} else if (code == KeyEvent.VK_RIGHT) {
					remote.east();
				} else if (code == KeyEvent.VK_B) {
					remote.boost(false);
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				int code = e.getKeyCode();
				// reset repeat protection
				resetPrevention(code);
				// handle event
				if (code == KeyEvent.VK_UP) {
					remote.north(true);
				} else if (code == KeyEvent.VK_DOWN) {
					remote.south(true);
				} else if (code == KeyEvent.VK_LEFT) {
					remote.west(true);
				} else if (code == KeyEvent.VK_RIGHT) {
					remote.east(true);
				} else if (code == KeyEvent.VK_B) {
					remote.boost(true);
				}
			}

			boolean[] downKeys = new boolean[0xFF];

			private synchronized boolean repeatPrevent(int index) {
				if (index >= 0 && index < downKeys.length && !downKeys[index]) {
					downKeys[index] = true;
					return false;
				}
				return true;
			}

			private synchronized void resetPrevention(int index) {
				if (index >= 0 && index < downKeys.length)
					downKeys[index] = false;
			}

		});
		setTitle("Remote Controller");
		setType(Type.UTILITY);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 200, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
	}

}
