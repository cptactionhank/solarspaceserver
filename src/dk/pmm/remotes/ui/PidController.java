package dk.pmm.remotes.ui;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import dk.pmm.ControlConstants;
import dk.pmm.communication.RemoteServer;
import dk.pmm.controls.pid.PidControl;
import dk.pmm.controls.pid.RemotePidControl;

@SuppressWarnings("serial")
public class PidController extends JFrame {

	private JPanel contentPane;
	private JLabel lblOffset;
	private JLabel lblPower;
	private JLabel lblKp;
	private JLabel lblKi;
	private JLabel lblKd;
	private JLabel lblScake;
	private JTextField txtOffset;
	private JTextField txtPower;
	private JTextField txtKp;
	private JTextField txtKi;
	private JTextField txtKd;
	private JTextField txtScale;
	private JPanel panel;
	private JButton btnSet;
	private JLabel lblDerivative;
	private JLabel lblIntegral;
	private JLabel lblError;
	private JLabel lblErrorValue;
	private JLabel lblIntegralvalue;
	private JLabel lblDerivativeValue;
	private JButton btnReadValues;

	private final RemoteServer remote;
	private final RemotePidControl controller;
	private final PidControl observer;
	private JLabel lblAdjustedPower;
	private JLabel lblAdjustedPowerValue;
	private JButton btnReset;
	private JLabel lblRightPower;
	private JLabel lblPowerRight;
	private JLabel lblLeftPowerValue;
	private JLabel lblPowerRightValue;

	/**
	 * Create the frame.
	 * 
	 * @param pidController
	 */
	public PidController(final RemoteServer remote) {
		this.remote = remote;
		this.controller = new RemotePidControl(ControlConstants.OFFSET_PID,
				remote.messenger);
		this.observer = new PidControl(ControlConstants.OFFSET_PID) {

			@Override
			public void log(int ch, Object... elements) {
				if (ControlConstants.CHANNEL_PID == ch && elements.length >= 4) {
					lblErrorValue.setText(elements[1].toString());
					lblIntegralvalue.setText(elements[2].toString());
					lblDerivativeValue.setText(elements[3].toString());
				}
				if (ControlConstants.CHANNEL_PID == ch && elements.length >= 5) {
					lblAdjustedPowerValue.setText(elements[4].toString());
				}
				if (ControlConstants.CHANNEL_PID == ch && elements.length >= 7) {
					lblLeftPowerValue.setText(elements[5].toString());
					lblPowerRightValue.setText(elements[6].toString());
				}
			}

			@Override
			public void setOffset(int value) {
				txtOffset.setText(Integer.toString(value));
			}

			@Override
			public void setPower(int value) {
				txtPower.setText(Integer.toString(value));
			}

			@Override
			public void setKp(float value) {
				txtKp.setText(Float.toString(value));
			}

			@Override
			public void setKi(float value) {
				txtKi.setText(Float.toString(value));
			}

			@Override
			public void setKd(float value) {
				txtKd.setText(Float.toString(value));
			}

			@Override
			public void setScale(float value) {
				txtScale.setText(Float.toString(value));
			}

			@Override
			public void report() {
				// we don't report anything when asked
			}

			@Override
			public void reset() {
				// we don't reset anything when asked
			}
		};

		setTitle("PID Remote Controller");
		setType(Type.UTILITY);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 536, 395);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 120, 116, 0 };
		gbl_contentPane.rowHeights = new int[] { 22, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 0.0, 1.0,
				Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		lblOffset = new JLabel("Offset");
		GridBagConstraints gbc_lblOffset = new GridBagConstraints();
		gbc_lblOffset.fill = GridBagConstraints.VERTICAL;
		gbc_lblOffset.anchor = GridBagConstraints.EAST;
		gbc_lblOffset.insets = new Insets(0, 0, 5, 5);
		gbc_lblOffset.gridx = 0;
		gbc_lblOffset.gridy = 0;
		contentPane.add(lblOffset, gbc_lblOffset);

		txtOffset = new JTextField();
		txtOffset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.setOffset(new Integer(txtOffset.getText()));
			}
		});
		GridBagConstraints gbc_txtOffset = new GridBagConstraints();
		gbc_txtOffset.insets = new Insets(0, 0, 5, 0);
		gbc_txtOffset.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtOffset.gridx = 1;
		gbc_txtOffset.gridy = 0;
		contentPane.add(txtOffset, gbc_txtOffset);
		txtOffset.setColumns(10);

		lblPower = new JLabel("Power");
		GridBagConstraints gbc_lblPower = new GridBagConstraints();
		gbc_lblPower.anchor = GridBagConstraints.EAST;
		gbc_lblPower.insets = new Insets(0, 0, 5, 5);
		gbc_lblPower.gridx = 0;
		gbc_lblPower.gridy = 1;
		contentPane.add(lblPower, gbc_lblPower);

		txtPower = new JTextField();
		txtPower.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.setPower(new Integer(txtPower.getText()));
			}
		});
		GridBagConstraints gbc_txtPower = new GridBagConstraints();
		gbc_txtPower.insets = new Insets(0, 0, 5, 0);
		gbc_txtPower.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPower.gridx = 1;
		gbc_txtPower.gridy = 1;
		contentPane.add(txtPower, gbc_txtPower);
		txtPower.setColumns(10);

		lblKp = new JLabel("KP");
		GridBagConstraints gbc_lblKp = new GridBagConstraints();
		gbc_lblKp.anchor = GridBagConstraints.EAST;
		gbc_lblKp.insets = new Insets(0, 0, 5, 5);
		gbc_lblKp.gridx = 0;
		gbc_lblKp.gridy = 2;
		contentPane.add(lblKp, gbc_lblKp);

		txtKp = new JTextField();
		txtKp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.setKp(new Float(txtKp.getText()));
			}
		});
		GridBagConstraints gbc_txtKp = new GridBagConstraints();
		gbc_txtKp.insets = new Insets(0, 0, 5, 0);
		gbc_txtKp.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtKp.gridx = 1;
		gbc_txtKp.gridy = 2;
		contentPane.add(txtKp, gbc_txtKp);
		txtKp.setColumns(10);

		lblKi = new JLabel("KI");
		GridBagConstraints gbc_lblKi = new GridBagConstraints();
		gbc_lblKi.anchor = GridBagConstraints.EAST;
		gbc_lblKi.insets = new Insets(0, 0, 5, 5);
		gbc_lblKi.gridx = 0;
		gbc_lblKi.gridy = 3;
		contentPane.add(lblKi, gbc_lblKi);

		txtKi = new JTextField();
		txtKi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.setKi(new Float(txtKi.getText()));
			}
		});
		GridBagConstraints gbc_txtKi = new GridBagConstraints();
		gbc_txtKi.insets = new Insets(0, 0, 5, 0);
		gbc_txtKi.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtKi.gridx = 1;
		gbc_txtKi.gridy = 3;
		contentPane.add(txtKi, gbc_txtKi);
		txtKi.setColumns(10);

		lblKd = new JLabel("KD");
		GridBagConstraints gbc_lblKd = new GridBagConstraints();
		gbc_lblKd.anchor = GridBagConstraints.EAST;
		gbc_lblKd.insets = new Insets(0, 0, 5, 5);
		gbc_lblKd.gridx = 0;
		gbc_lblKd.gridy = 4;
		contentPane.add(lblKd, gbc_lblKd);

		txtKd = new JTextField();
		txtKd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.setKd(new Float(txtKd.getText()));
			}
		});
		GridBagConstraints gbc_txtKd = new GridBagConstraints();
		gbc_txtKd.insets = new Insets(0, 0, 5, 0);
		gbc_txtKd.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtKd.gridx = 1;
		gbc_txtKd.gridy = 4;
		contentPane.add(txtKd, gbc_txtKd);
		txtKd.setColumns(10);

		lblScake = new JLabel("Scale");
		GridBagConstraints gbc_lblScake = new GridBagConstraints();
		gbc_lblScake.anchor = GridBagConstraints.EAST;
		gbc_lblScake.insets = new Insets(0, 0, 5, 5);
		gbc_lblScake.gridx = 0;
		gbc_lblScake.gridy = 5;
		contentPane.add(lblScake, gbc_lblScake);

		txtScale = new JTextField();
		txtScale.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.setScale(new Float(txtScale.getText()));
			}
		});
		GridBagConstraints gbc_txtScale = new GridBagConstraints();
		gbc_txtScale.insets = new Insets(0, 0, 5, 0);
		gbc_txtScale.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtScale.gridx = 1;
		gbc_txtScale.gridy = 5;
		contentPane.add(txtScale, gbc_txtScale);
		txtScale.setColumns(10);

		lblError = new JLabel("Error");
		GridBagConstraints gbc_lblError = new GridBagConstraints();
		gbc_lblError.anchor = GridBagConstraints.EAST;
		gbc_lblError.insets = new Insets(0, 0, 5, 5);
		gbc_lblError.gridx = 0;
		gbc_lblError.gridy = 6;
		contentPane.add(lblError, gbc_lblError);

		lblErrorValue = new JLabel("<Error Value>");
		GridBagConstraints gbc_lblErrorValue = new GridBagConstraints();
		gbc_lblErrorValue.anchor = GridBagConstraints.WEST;
		gbc_lblErrorValue.insets = new Insets(0, 0, 5, 0);
		gbc_lblErrorValue.gridx = 1;
		gbc_lblErrorValue.gridy = 6;
		contentPane.add(lblErrorValue, gbc_lblErrorValue);

		lblIntegral = new JLabel("Integral");
		GridBagConstraints gbc_lblIntegral = new GridBagConstraints();
		gbc_lblIntegral.anchor = GridBagConstraints.EAST;
		gbc_lblIntegral.insets = new Insets(0, 0, 5, 5);
		gbc_lblIntegral.gridx = 0;
		gbc_lblIntegral.gridy = 7;
		contentPane.add(lblIntegral, gbc_lblIntegral);

		lblIntegralvalue = new JLabel("<Integral Value>");
		GridBagConstraints gbc_lblIntegralvalue = new GridBagConstraints();
		gbc_lblIntegralvalue.anchor = GridBagConstraints.WEST;
		gbc_lblIntegralvalue.insets = new Insets(0, 0, 5, 0);
		gbc_lblIntegralvalue.gridx = 1;
		gbc_lblIntegralvalue.gridy = 7;
		contentPane.add(lblIntegralvalue, gbc_lblIntegralvalue);

		lblDerivative = new JLabel("Derivative");
		GridBagConstraints gbc_lblDerivative = new GridBagConstraints();
		gbc_lblDerivative.anchor = GridBagConstraints.EAST;
		gbc_lblDerivative.insets = new Insets(0, 0, 5, 5);
		gbc_lblDerivative.gridx = 0;
		gbc_lblDerivative.gridy = 8;
		contentPane.add(lblDerivative, gbc_lblDerivative);

		lblDerivativeValue = new JLabel("<Derivative Value>");
		GridBagConstraints gbc_lblDerivativeValue = new GridBagConstraints();
		gbc_lblDerivativeValue.anchor = GridBagConstraints.WEST;
		gbc_lblDerivativeValue.insets = new Insets(0, 0, 5, 0);
		gbc_lblDerivativeValue.gridx = 1;
		gbc_lblDerivativeValue.gridy = 8;
		contentPane.add(lblDerivativeValue, gbc_lblDerivativeValue);

		lblAdjustedPower = new JLabel("Adjusted power");
		GridBagConstraints gbc_lblAdjustedPower = new GridBagConstraints();
		gbc_lblAdjustedPower.anchor = GridBagConstraints.EAST;
		gbc_lblAdjustedPower.insets = new Insets(0, 0, 5, 5);
		gbc_lblAdjustedPower.gridx = 0;
		gbc_lblAdjustedPower.gridy = 9;
		contentPane.add(lblAdjustedPower, gbc_lblAdjustedPower);

		lblAdjustedPowerValue = new JLabel("<Adjusted Power Value>");
		GridBagConstraints gbc_lblAdjustedPowerValue = new GridBagConstraints();
		gbc_lblAdjustedPowerValue.anchor = GridBagConstraints.WEST;
		gbc_lblAdjustedPowerValue.insets = new Insets(0, 0, 5, 0);
		gbc_lblAdjustedPowerValue.gridx = 1;
		gbc_lblAdjustedPowerValue.gridy = 9;
		contentPane.add(lblAdjustedPowerValue, gbc_lblAdjustedPowerValue);
		
		lblRightPower = new JLabel("Power Left");
		GridBagConstraints gbc_lblRightPower = new GridBagConstraints();
		gbc_lblRightPower.anchor = GridBagConstraints.EAST;
		gbc_lblRightPower.insets = new Insets(0, 0, 5, 5);
		gbc_lblRightPower.gridx = 0;
		gbc_lblRightPower.gridy = 10;
		contentPane.add(lblRightPower, gbc_lblRightPower);
		
		lblLeftPowerValue = new JLabel("<Left Power Value>");
		GridBagConstraints gbc_lblLeftPowerValue = new GridBagConstraints();
		gbc_lblLeftPowerValue.anchor = GridBagConstraints.WEST;
		gbc_lblLeftPowerValue.insets = new Insets(0, 0, 5, 0);
		gbc_lblLeftPowerValue.gridx = 1;
		gbc_lblLeftPowerValue.gridy = 10;
		contentPane.add(lblLeftPowerValue, gbc_lblLeftPowerValue);
		
		lblPowerRight = new JLabel("Power Right");
		GridBagConstraints gbc_lblPowerRight = new GridBagConstraints();
		gbc_lblPowerRight.anchor = GridBagConstraints.EAST;
		gbc_lblPowerRight.insets = new Insets(0, 0, 5, 5);
		gbc_lblPowerRight.gridx = 0;
		gbc_lblPowerRight.gridy = 11;
		contentPane.add(lblPowerRight, gbc_lblPowerRight);
		
		lblPowerRightValue = new JLabel("<Right Power Value>");
		GridBagConstraints gbc_lblPowerRightValue = new GridBagConstraints();
		gbc_lblPowerRightValue.anchor = GridBagConstraints.WEST;
		gbc_lblPowerRightValue.insets = new Insets(0, 0, 5, 0);
		gbc_lblPowerRightValue.gridx = 1;
		gbc_lblPowerRightValue.gridy = 11;
		contentPane.add(lblPowerRightValue, gbc_lblPowerRightValue);

		panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.RIGHT);
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 1;
		gbc_panel.gridy = 13;
		contentPane.add(panel, gbc_panel);

		btnSet = new JButton("Commit values");
		btnSet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					controller.setKd(new Float(txtKd.getText()));
					Thread.sleep(200);
					controller.setKi(new Float(txtKi.getText()));
					Thread.sleep(200);
					controller.setKp(new Float(txtKp.getText()));
					Thread.sleep(200);
					controller.setOffset(new Integer(txtOffset.getText()));
					Thread.sleep(200);
					controller.setPower(new Integer(txtPower.getText()));
					Thread.sleep(200);
					controller.setScale(new Float(txtScale.getText()));
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		});

		btnReadValues = new JButton("Read values");
		btnReadValues.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.report();
			}
		});

		btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.reset();
			}
		});
		panel.add(btnReset);
		panel.add(btnReadValues);
		panel.add(btnSet);
	}

	@Override
	public void setVisible(boolean b) {
		super.setVisible(b);
		if (b) {
			// listen for incoming messages
			remote.addObserver(observer);
			// ask for initial value
			controller.report();
		} else {
			remote.removeObserver(observer);
		}
	}

}
