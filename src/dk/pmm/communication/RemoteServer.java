package dk.pmm.communication;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import lejos.pc.comm.NXTComm;
import lejos.pc.comm.NXTCommException;
import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTInfo;
import dk.pmm.Base;
import dk.pmm.messaging.Messenger;
import dk.pmm.messaging.RemoteMessenger;
import dk.pmm.messaging.events.MessagingEventObserver;

public class RemoteServer extends Base {

	// remote connection object
	private final NXTComm connection;
	// remote messenger lock
	protected final Object LOCK = new Object();
	// remote messenger client
	public volatile Messenger messenger;
	// listener thread
	protected Thread listener;

	public RemoteServer() throws NXTCommException {
		connection = NXTCommFactory.createNXTComm(NXTCommFactory.BLUETOOTH);
	}

	public boolean connect(NXTInfo info) {
		// initialize as not connected
		boolean connected = false;
		// make sure we have disconnected at first
		disconnect();
		try {
			// open the connection
			connected = connection.open(info);
		} catch (Throwable e) {
			// package runtime exceptions in an NXT exception
			exception(e);
		}
		// if connected we want to do stuff
		if (connected) {
			// get IO streams
			final InputStream input = connection.getInputStream();
			final OutputStream output = connection.getOutputStream();
			// save a reference for this instance
			final RemoteServer _this = this;
			// make sure no one has the lock before initializing the listener
			synchronized (LOCK) {
				// create and start listener thread
				_this.listener = new Thread(new Runnable() {

					@Override
					public void run() {
						synchronized (LOCK) {
							messenger = _this.messenger;
						}
						// make sure to only listen if the messenger is
						// available
						if (messenger != null) {
							try {
								messenger.listen();
							} catch (IOException e) {
								// this is likely to be a stream closed
							} catch (Exception e) {
								_this.exception(e);
							}
						}
					}
				}, "Listener thread");

				// setup the new remote messenger
				_this.messenger = new RemoteMessenger(input, output) {

					protected void onKillCommand() {
						_this.disconnect();
					};

				};
				// add a default observer to this
				_this.messenger.addObserver(new MessagingEventObserver() {

					@Override
					protected void onVendorCommand(int identifier, Object object) {
						_this.onVendorCommand(identifier, object);
					}

					@Override
					protected void onMessageCommand(String message) {
						_this.onMessageCommand(message);
					}

					@Override
					public void log(int channel, Object... elements) {
						_this.log(channel, elements);
					}

				});

				// setup and start the listener thread
				_this.listener.setDaemon(true);
				_this.listener.start();
			}
		}

		return connected;
	}

	public void disconnect() {
		// disable the messenger
		synchronized (LOCK) {
			// interrupt the listener thread
			if (this.listener != null)
				this.listener.interrupt();
			// remote messenger instance
			if (this.messenger != null) {
				this.messenger.removeObservers();
				this.messenger = null;
			}
			// close the connection
			try {
				connection.close();
			} catch (Throwable e) {
				exception(e);
			}
		}
	}

	public void addObserver(MessagingEventObserver observer) {
		synchronized (LOCK) {
			if (messenger != null) {
				messenger.addObserver(observer);
			}
		}
	}

	public void removeObserver(MessagingEventObserver observer) {
		synchronized (LOCK) {
			if (messenger != null) {
				messenger.removeObserver(observer);
			}
		}
	}

	public void kill() {
		synchronized (LOCK) {
			try {
				if (messenger != null)
					messenger.kill();
			} catch (IOException e) {
				exception(e);
			}
		}
	}

	public void message(String message) {
		synchronized (LOCK) {
			try {
				if (messenger != null)
					messenger.message(message);
			} catch (IOException e) {
				exception(e);
			}
		}
	}

	public Messenger getMessenger() {
		synchronized (LOCK) {
			return messenger;
		}
	}

}
